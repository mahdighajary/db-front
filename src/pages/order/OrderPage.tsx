import React, { ChangeEvent, useContext } from 'react';
import {
  Button, Input, Typography,
} from '@material-ui/core';
import style from './getuserinfo.module.scss';
import { InputPhoneNumber } from './InputPhoneNumber.component';
import { axiosInstance } from '../../axiosConfig';

interface GetUserInfoProps {
  onFinish?: (fisrtname: string, lastname:string, referer: string)=>void;
}

export const OrderPage: React.FC<GetUserInfoProps> = (props) => {
  const [firstname, setFirstname] = React.useState<string>('');
const [referrer, setReferrer] = React.useState<string>('');

  const params = new URLSearchParams(window.location.search)

  const cartId = params.get('cartId')

  const handleFirstnameChange = (event: ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    setFirstname(value);
  };


  const handleReferrerChange = (newValue: string) => {
    setReferrer(newValue);
  };

  const handleSubmit = () => {
    if (firstname.length > 0) {
      axiosInstance.post('/order', {
        address: firstname,
        phoneNumber: referrer,
        cartId: Number(cartId),
      })
    }
  };

  return (
    <div className={style.root}>
      <div className={style.titleBox}>
        <div>
          <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18.6673 2.66699H8.00065C7.29341 2.66699 6.61513 2.94794 6.11503 3.44804C5.61494 3.94814 5.33398 4.62641 5.33398 5.33366V26.667C5.33398 27.3742 5.61494 28.0525 6.11503 28.5526C6.61513 29.0527 7.29341 29.3337 8.00065 29.3337H24.0006C24.7079 29.3337 25.3862 29.0527 25.8863 28.5526C26.3864 28.0525 26.6673 27.3742 26.6673 26.667V10.667L18.6673 2.66699Z" stroke="#6F2CF6" strokeWidth="3" strokeLinecap="square" strokeLinejoin="round" />
            <path d="M21.3327 22.667H10.666" stroke="#F5B414" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M21.3327 17.333H10.666" stroke="#F5B414" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M13.3327 12H11.9993H10.666" stroke="#F5B414" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
            <path d="M18.666 2.66699V10.667H26.666" stroke="#6F2CF6" strokeWidth="2" strokeLinecap="square" strokeLinejoin="round" />
          </svg>
        </div>
        <div className={style.title}>
          اطلاعات سفارش
        </div>
      </div>
      <div>
        <div className={style.inputLabel}>
          <Typography>آدرس تحویل</Typography>
        </div>
        <div className={style.inputControl}>
          <Input
            className={style.input}
            value={firstname}
            onChange={handleFirstnameChange}
            disableUnderline
          />
        </div>
      </div>
      {/* <div>
        <div className={style.inputLabel}>
          <Typography>last-name</Typography>
        </div>
        <div className={style.inputControl}>
          <Input
            className={style.input}
            value={lastname}
            onChange={handleLastnameChange}
            disableUnderline
          />
        </div>
      </div> */}
      <div>
        <div className={style.inputLabel}>
          <Typography>شماره موبایل</Typography>
        </div>
        <div className={style.inputControl}>
          <InputPhoneNumber
            className={style.input}
            value={referrer}
            onChange={handleReferrerChange}
          />
        </div>
        <div>
          <Typography className={style.phoneNumberError} />
        </div>
      </div>
      <div>
        <Button
          className={style.button}
          onClick={handleSubmit}
          variant="contained"
          color="primary"
        >
          تایید سفارش
        </Button>
      </div>
    </div>
  );
};
