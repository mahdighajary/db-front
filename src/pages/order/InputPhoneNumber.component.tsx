import { Input, InputAdornment, Typography } from '@material-ui/core';
import React, { ChangeEvent } from 'react';
import style from './inputPhone.module.scss';

interface Props {
  className?: string;
  onChange: (newValue: string) => void;
  value: string;
  inputRef?: React.MutableRefObject<HTMLInputElement>;
  onKeyDown?: React.KeyboardEventHandler<HTMLTextAreaElement | HTMLInputElement>;
}

const spaceInNumber = (phone: string) => (phone.length > 5
  ? phone.replace(/(\d{2})(\d{3})(\d{1,4})/, '$1  $2  $3')
  : (phone.length > 2 ? phone.replace(/(\d{2})(\d{1,3})/, '$1  $2') : phone));

export const InputPhoneNumber: React.FC<Props> = (props) => {
  const phoneNumber = props.value.startsWith('09') ? props.value.replace('09', '') : props.value;
  const handlePhoneNumberChange = (event: ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value.replace(/ /g, '');
    if (value.length === 0) {
      props.onChange('09');
      return;
    }
    if (Number(value) && value.length < 10) props.onChange(`09${value}`);
  };
  const phoneAdornment = (
    <InputAdornment position="start">
      <span className={style.phoneAdornment}>
        <svg width="35" height="24" viewBox="0 0 35 24" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M17 2H7C5.89543 2 5 2.89543 5 4V20C5 21.1046 5.89543 22 7 22H17C18.1046 22 19 21.1046 19 20V4C19 2.89543 18.1046 2 17 2Z" stroke="#4E4E4E" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
          <path d="M12 18H12.01" stroke="#4E4E4E" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
          <path d="M34 24V-1.07288e-06" stroke="#DDDDDD" />
        </svg>
      </span>
      <Typography>09</Typography>
    </InputAdornment>
  );
  return (
    <Input
      className={props.className}
      dir="ltr"
      startAdornment={phoneAdornment}
      value={spaceInNumber(phoneNumber)}
      onChange={handlePhoneNumberChange}
      onKeyDown={props.onKeyDown}
      disableUnderline
      inputRef={props.inputRef}
      placeholder="12 345 6789"
    />
  );
};
