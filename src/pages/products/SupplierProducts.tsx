import React, { useContext } from 'react';
import { Grid } from "@material-ui/core";
import styles from "./products.module.scss";
import { ProductCard } from "./ProductCard";
import { Product } from './Products';

export const returnSearchProductItem = (item: Product, i: number) => (
  <Grid item xs={12} sm={6} md={4} className={styles.productWrapper} key={i}>
    <ProductCard
      id={item.id}
      image={item.image}
      title={item.title}
      url={item.title}
      price={item.price}
    />
  </Grid>
);

export const returnProductItem = (item: Product) => (
  <ProductCard
    id={item.id}
    image={item.image}
    title={item.title}
    url={item.title}
    price={item.price}
  />
);

export const SupplierProducts: React.FC<{products: Product[]}> = (props) => {

  return (
    <div>
      <h2 className={styles.header2}>products</h2>
      <Grid container className={styles.root}>
        {props.products.map((item) => returnProductItem(item))}
      </Grid>
    </div>
  );
};
