/* eslint-disable no-restricted-syntax */
import React, { FC, useState } from "react";
import {
  Box, FormControl, Grid, InputLabel, makeStyles, MenuItem, Select, Theme,
} from "@material-ui/core";
import CircularProgress from '@material-ui/core/CircularProgress';

// import qs from 'query-string';
// import { useRouter } from "next/router";
// import Pagination from "./partListPagination.component";
// import { Preloader } from "../placeholder";
import { returnSearchProductItem } from "./SupplierProducts";
import { useQuery } from "react-query";
import Header from "../../Header";
import { axiosInstance } from "../../axiosConfig";

export interface Product {
  id: number;
  title: string;
  summary: string;
  price: number;
  stock: number;
  image: string;
}

const useStyles = makeStyles((theme: Theme) => (
  {
    table: {
      width: '100%',
      background: 'none',
      boxShadow: 'none',

    },
    paginationSelect: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    paginationButton: {
      marginLeft: 0,
    },
    loadingBox: {
      width: '100%',
      textAlign: 'center',
    },
    loading: {},
    listItem: {
      padding: 0,
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
    },
  }));

const getProducts = async (orderBy: OrderBy) => {
  const res = await axiosInstance.get<Product[]>(`/products?orderBy=${orderBy}`)
  return res.data
}

enum OrderBy {
  Date,
  MostExpensive,
  LeastExpensive,
}

export const ProductsPage: FC = (props) => {
  const classes = useStyles();

  const [orderBy, setOrderBy] = useState<OrderBy>(OrderBy.Date)

  const handleOrderBy = (event: React.ChangeEvent<{
    name?: string | undefined;
    value: unknown;
}>, child: React.ReactNode) => {
    setOrderBy(event.target.value as OrderBy)
  }

  const { isLoading, data } = useQuery(['products', orderBy], () => getProducts(orderBy), {
    initialData: [] as Product[],
    refetchOnWindowFocus: false,
  });

  return (
    <Box>
      <Header />
      <FormControl variant="outlined" style={{ marginTop: 130, width: 150,
         float: 'right', marginRight: 25 }}>
        <InputLabel>Age</InputLabel>
        <Select
          value={orderBy}
          label="Age"
          onChange={handleOrderBy}
        >
          <MenuItem value={OrderBy.Date}>زمانی</MenuItem>
          <MenuItem value={OrderBy.MostExpensive}>گرانترین</MenuItem>
          <MenuItem value={OrderBy.LeastExpensive}>ارزانترین</MenuItem>
        </Select>
      </FormControl>
      <Box style={{ width: "98%" }}>
        <Grid container classes={{ root: classes.listItem }} spacing={1}>
          {!data ? (<CircularProgress variant="determinate" value={25} />)
            : (data.length
              ? data.map((item, i) => (
                returnSearchProductItem(item, i)
              )) : null)}
        </Grid>
        {/* {pagination} */}
      </Box>
    </Box>
  );
};
