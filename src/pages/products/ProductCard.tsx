import React, { useContext } from 'react';
import { Button } from "@material-ui/core";
import { CartDispatchContext, addToCart } from "../../contexts/cart";
import styles from "./products.module.scss";

export function toFarsiNumber(n: any) {
  const farsiDigits = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];

  return n
    .toString()
    .split('')
    .map((x: any) => farsiDigits[x])
    .join('');
}

export const numberWithCommas = (x: number) => x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

interface Props {
  id: number
  image: string,
  title: string,
  url: string,
  price: number,
}

const returnPrice = (price: number) => (
  <div className={styles.productPrice}>
    <p className={styles.productPriceTag}>{`${toFarsiNumber(numberWithCommas(price))}`}</p>
    <p className={styles.productPriceCurrency}>تومان</p>
  </div>
);

export const ProductCard: React.FC<Props> = (props) => {
  const dispatch = useContext(CartDispatchContext);
  const handleAddToCart = () => {
    const product = { ...props, quantity: 1 };
    addToCart(dispatch, product);
  }
  return (
    <div onClick={handleAddToCart} className={styles.productContent}> 
      <img src={props.image || "https://picsum.photos/300"} title={props.title} alt={props.title} className={styles.productImg} />
      <div className={styles.productTags}>
        <div className={styles.productTitle}>{props.title}</div>
        <div style={{ display: 'flex', justifyContent: 'space-between', marginTop: 25 }}>
          <Button className={styles.addCartButton}> اضافه کردن به سبد خرید </Button>
          {returnPrice(props.price)}
        </div>
      </div>
      {/* <Button variant="text" color="primary" className={styles.resultButton}>show-details</Button> */}
    </div>
  );
};
