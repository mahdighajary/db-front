import { createStyles, makeStyles, Theme } from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) => createStyles({
  main: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  resultPagination: {
    width: "100%",
    alignItems: "center",
    backgroundColor: "initial",
    display: "flex",
    justifyContent: "space-between",
    padding: "20px 10px",
    "& > button": {
      lineHeight: "26px",
      borderRadius: 5,
      padding: 0,
      margin: 10,
      opacity: 1,
      border: "1px solid #000",
      height: 28,
    },
    "& p": {
      color: "#000",
    },
  },
  paginationNumbers: {
    display: 'flex',
  },
  paginationBox: {
    borderRadius: "25px",
    boxSizing: "border-box",
    boxShadow: "none",
    cursor: "pointer",
    fontWeight: 600,
    backgroundColor: "#fff",
    width: 'fit-content',
    color: "#000",
    display: "flex",
    justifyContent: 'center',
    alignItems: 'center',
    height: "50px",
    lineHeight: "50px",
    margin: 10,
    maxWidth: "100%",
    overflow: "hidden",
    outline: "none",
    padding: "0 28px",
    textAlign: 'center',
    textOverflow: "ellipsis",
    transition: "background-color 0.2s, opacity 0.2s",
    whiteSpace: "nowrap",
  },
  cardItem: {
    width: 200,
    margin: 5,
    border: '1px solid #e3e3e2',
    borderRadius: '3px',
  },
  showPart: {
    textDecoration: 'none',
    '&:hover': {
      textDecoration: 'none',
    },
  },
  header: {
    paddingRight: theme.spacing(1),
    paddingLeft: theme.spacing(1),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(0.5),
    lineHeight: 20,
    minHeight: 40,
    alignItems: 'start',
  },
  headerTitle: {
    fontSize: 13,
  },
  itemImage: {
    height: 160,
    maxHeight: 160,
    maxWidth: 160,
    marginBottom: theme.spacing(4),
    position: 'relative',
  },
  partCaption: {
    marginBottom: 10,
  },
  partPrice: {
    color: 'inherit',
  },
  button: {
    marginTop: theme.spacing(1),
    backgroundColor: theme.palette.common.white,
    border: '1px solid purple',
    width: '100%',
    fontSize: 13,
    color: 'purple',
    '&:hover': {
      backgroundColor: "#eee",
    },
  },
  products: {
    alignSelf: 'flex-start',
    fontWeight: 'bold',
    fontSize: 18,
  },
  link: {
    textDecoration: 'none',
    color: theme.palette.grey["900"],
  },
  cardContent: {
    padding: theme.spacing(2),
  },
  manufacturer: {
    minHeight: theme.spacing(3),
  },
}));

export default useStyles;
