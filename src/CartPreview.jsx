import React, { useContext } from "react";
import classNames from "classnames";
import {
  CartStateContext,
  CartDispatchContext,
  removeFromCart,
  toggleCartPopup
} from "./contexts/cart";
import { toFarsiNumber } from "./pages/products/ProductCard";
import { axiosInstance } from "./axiosConfig";

const CartPreview = () => {
  const { items, isCartOpen } = useContext(CartStateContext);
  const dispatch = useContext(CartDispatchContext);

  const handleRemove = (productId) => {
    return removeFromCart(dispatch, productId);
  };

  const handleProceedCheckout = async () => {
    const result = await axiosInstance.post('/cart', items.map((item) => ({id: item.id, qty: item.quantity})))
    toggleCartPopup(dispatch);
    window.location.href = `?cartId=${result.data.cartId}`
  };

  return (
    <div className={classNames("cart-preview", { active: isCartOpen })}>
      <ul className="cart-items">
        {items.map((product) => {
          return (
            <li className="cart-item" key={product.title}>
              <img className="product-image" src={product.image} />
              <div className="product-info">
                <p className="product-name">{product.title}</p>
                <p className="product-price">{toFarsiNumber(product.price)} تومان</p>
              </div>
              <div className="product-total">
                <p className="quantity">
                  {`تعداد: ${toFarsiNumber(product.quantity)}`}
                </p>
                <p className="amount">{toFarsiNumber(product.quantity * product.price)} تومان</p>
              </div>
              <button
                className="product-remove"
                onClick={() => handleRemove(product.id)}
              >
                ×
              </button>
            </li>
          );
        })}
      </ul>
      <div className="action-block">
        <button
          type="button"
          className={classNames({ disabled: items && items.length === 0 })}
          onClick={handleProceedCheckout}
        >
          نهایی کردن خرید
        </button>
      </div>
    </div>
  );
};

export default CartPreview;
