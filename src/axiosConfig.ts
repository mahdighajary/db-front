import axios, { AxiosRequestConfig } from 'axios';

const baseURL = "http://localhost:7070";

const axiosDefaultConfigs: AxiosRequestConfig = {
  baseURL,
};

export const axiosInstance = axios.create(axiosDefaultConfigs);
