import React, { useState } from "react";
import logo from "./logo.svg";
import { QueryClient, QueryClientProvider } from "react-query";
import "./App.css";
import "./assets/scss/style.scss";
import { ProductsPage } from "./pages/products/Products";
import { StylesProvider } from "@material-ui/core";
import CartProvider from "./contexts/cart";
import CheckoutProvider from "./contexts/checkout";
import { OrderPage } from "./pages/order/OrderPage";

function App() {
  // const [onOrder, setOnOrder] = useState(false);
  const params = new URLSearchParams(window.location.search);

  const onOrder = params.get("cartId");

  const queryClient = new QueryClient();

  return (
    <div className="App">
      <QueryClientProvider client={queryClient}>
        <StylesProvider injectFirst>
          <CheckoutProvider>
            <CartProvider>
              {onOrder ? <OrderPage /> : <ProductsPage />}
            </CartProvider>
          </CheckoutProvider>
        </StylesProvider>
      </QueryClientProvider>
    </div>
  );
}

export default App;
